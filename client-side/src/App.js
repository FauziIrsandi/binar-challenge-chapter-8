import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";
import CreateForm from "./form/createForm";
import UpdateForm from "./form/updateForm";
import SearchForm from "./form/searchForm";
import "./styles.css";

export default function App() {
  return (
    <div className="App">
      <h1>Aplikasi simpel form ReactJS </h1>
      <Router>
        <ul>
          <li>
            <Link to="/">To Home (Create new Player)</Link>
          </li>
          <li>
            <Link to="/update">To Edit player</Link>
          </li>
          <li>
            <Link to="/search">To Search player</Link>
          </li>
        </ul>
        <Routes>
          <Route exact path="/" element={<CreateForm />} />
          <Route exact path="/update" element={<UpdateForm />} />
          <Route exact path="/search" element={<SearchForm />} />
        </Routes>
      </Router>
    </div>
  );
}
