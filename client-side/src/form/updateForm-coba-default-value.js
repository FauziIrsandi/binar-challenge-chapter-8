import React from "react";
import {
  withStyles,
  Card,
  CardContent,
  CardActions,
  TextField,
  MenuItem,
  Button
} from "@material-ui/core";
import validationsForm from "../validations/validationSchema";
import { useFormik } from "formik";
import * as yup from "yup";

const styles = () => ({
    card: {
      maxWidth: 420,
      marginTop: 50
    },
    container: {
      display: "Flex",
      justifyContent: "center"
    },
    actions: {
      float: "right"
    }
  });
  
  const gameCategory = [
    {
      value: "firstPersonShooter",
      label: "First Person Shooter"
    },
    {
      value: "simulation",
      label: "Simulation"
    },
    {
      value: "action",
      label: "Action"
    },
    {
      value: "adventure",
      label: "Adventure"
    },
    {
      value: "horrorGame",
      label: "Horror Game"
    },
    {
      value: "story",
      label: "Story Game"
    },
    {
      value: "rolePlayingGame",
      label: "RPG"
    },
    {
      value: "openWorld",
      label: "Open World"
    },
  ];

  const UpdateForm = (props) => {
    const [name, setName] = React.useState('Nama Saya');
    const [username, setUsername] = React.useState('joomebody69');
    // const [someName, setName] = React.useState('Somebody');
    // const [someName, setName] = React.useState('Somebody');
    // const [someName, setName] = React.useState('Somebody');
    // const [someName, setName] = React.useState('Somebody');
    // const [someName, setName] = React.useState('Somebody');
    // const [someName, setName] = React.useState('Somebody');
    const formik = useFormik({
      initialValues: {
        name: name,
        username: username,
      },
      validationSchema: yup.object().shape(validationsForm), // validation here
      onSubmit: (values) => {
        setTimeout(() => {
            // submit to the server
            alert("Data Player yang DIUBAH => \n" + JSON.stringify(values, null, 2));
        }, 1000);
      }
    })

    const {
        classes,
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit,
        handleReset
    } = props;
  
    return (
      <>
        <form onSubmit={handleSubmit}>
          <input type="text" name="name" onChange={handleChange} values={values.name} />
        </form>
      </>
    );
  }

//   export default withStyles(styles)(UpdateForm);
  export default UpdateForm;