import React from "react";
import {
  withStyles,
  Card,
  CardContent,
  CardActions,
  TextField,
  MenuItem,
  Button
} from "@material-ui/core";
import validationsForm from "../validations/validationSchema";
import { withFormik } from "formik";
import * as yup from "yup";

const styles = () => ({
  card: {
    maxWidth: 420,
    marginTop: 50
  },
  container: {
    display: "Flex",
    justifyContent: "center"
  },
  actions: {
    float: "right"
  }
});

const experiences = [
    {
        value: "ironRank",
        label: "Iron Rank"
    },
    {
        value: "copperRank",
        label: "Copper Rank"
    },
    {
        value: "silverRank",
        label: "Silver Rank"
    },
    {
        value: "goldRank",
        label: "Gold Rank"
    },
    {
        value: "diamondRank",
        label: "Diamond Rank"
    },
    {
        value: "platinumRank",
        label: "Platinum Rank"
    },
    {
        value: "amethystRank",
        label: "Amethyst Rank"
    },
    {
        value: "sapphireRank",
        label: "Sapphire Rank"
    },
];

const form = props => {
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset
  } = props;

  return (
    <>
      <h2>Cari player</h2>
      <div className={classes.container}>
        <form onSubmit={handleSubmit}>
          <Card className={classes.card}>
            <CardContent>
              <TextField
                id="username"
                label="Username"
                // defaultValue={"cobaAjajjj"}
                value={values.username}
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={touched.username ? errors.username : ""}
                error={touched.username && Boolean(errors.username)}
                margin="dense"
                variant="outlined"
                fullWidth />
              <TextField
                id="email"
                label="Email"
                type="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={touched.email ? errors.email : ""}
                error={touched.email && Boolean(errors.email)}
                margin="dense"
                variant="outlined"
                fullWidth />
              <TextField
                select
                id="experience"
                label="Experience"
                value={values.experience}
                onChange={handleChange("experience")}
                // helperText={touched.experience ? errors.experience : ""}
                // error={touched.experience && Boolean(errors.experience)}
                margin="dense"
                variant="outlined"
                fullWidth
              >
                {experiences.map(option => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
              <TextField
                id="level"
                label="Level"
                type="level"
                value={values.level}
                onChange={handleChange}
                onBlur={handleBlur}
                // helperText={touched.level ? errors.level : ""}
                // error={touched.level && Boolean(errors.level)}
                margin="dense"
                variant="outlined"
                fullWidth />
            </CardContent>
            <CardActions className={classes.actions}>
              <Button type="submit" color="primary" disabled={isSubmitting}>
                SUBMIT
              </Button>
              <Button color="secondary" onClick={handleReset}>
                CLEAR
              </Button>
            </CardActions>
          </Card>
        </form>
      </div>
    </>
  );
};

const Form = withFormik({
  mapPropsToValues: ({
    username,
    email,
    experience,
    level,
  }) => {
    return {
      username: username || "",
      email: email || "",
      experience: experience || "",
      level: level || "",
    };
  },

//   validationSchema: yup.object().shape(validationsForm),

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      // submit to the server
      alert("Data Player yang DICARI => \n" + JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  }
})(form);

export default withStyles(styles)(Form);
