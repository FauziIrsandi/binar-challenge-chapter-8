import * as yup from "yup";

const validationsForm = {
  name: yup.string().required("Required"),
  surname: yup.string().required("Required"),
  email: yup
    .string()
    .email("Enter a valid email")
    .required("Email is required"),
  game: yup.string().required("Select your game category"),
  password: yup
    .string()
    .min(8, "Password must contain at least 8 characters")
    .required("Enter your password"),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref("password")], "Password does not match")
    .required("Confirm your password"),
  username: yup
    .string()
    .min(8, "Username must be at least 8 characters")
    .required("Website is required"),
  // experience: yup.string().required("Select your experience"),
  // level: yup.string().required("Please input your level"),
};

export default validationsForm;
